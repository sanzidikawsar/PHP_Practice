<?php

class Practice{
    public function __construct($title = '')
    {
        echo "Hi, $title This is constructor";
    }

    public function setData($a = '',$b = ''){
        $this->a = $a;
        $this->b = $b;
    }

    public function sum($sumResult = ''){
        $sumResult = $this->a + $this->b;
        return $sumResult;
    }

    public function getData(){
        echo  $this->sum();
    }
}

$obj = new Practice("BD");
echo "</br>";

$obj->setData(22,78);
$obj->getData();
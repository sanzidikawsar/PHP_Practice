<?php

/**
 * Created by PhpStorm.
 * User: Web App Develop PHP
 * Date: 12/31/2016
 * Time: 12:22 PM
 */
class constructor
{
    public function __construct($title = '')
    {
        $title = "It's from class1 constructor </br>";
        return $title;
    }
}

class cls2 extends constructor{
    public function __construct($title = '')
    {
        $this->title = $title;
        echo $title;

        $title2 = "It's from class2 constructor";
        echo $title2;
    }
}

$obj = new cls2();
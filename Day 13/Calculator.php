<html>
<title> Calculator</title>

<header>
    <h3> Enter the values: </h3>
</header>

<body>
<form action="result.php" method="POST">
    1st Value: </br><input type="float" name="val1"></br>
    2nd Value: </br><input type="float" name="val2"></br>

    <input type="submit" name="type" value="Add">
    <input type="submit" name="type" value="Substract">
    <input type="submit" name="type" value="Multiply">
    <input type="submit" name="type" value="Divide"></form>
</body>


<?php

class Calculator
{
    public function __construct($val1='',$val2='')
    {
        $this->val1=$val1;
        $this->val2=$val2;
    }

    public function add(){
        return $this->val1 +  $this->val2;
    }

    public function substract(){
        return $this->val1 -  $this->val2;
    }

    public function divide(){
        return $this->val1 /  $this->val2;
    }

    public function multiply(){
        return $this->val1 *  $this->val2;
    }

}
?>
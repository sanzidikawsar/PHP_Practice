<?php


abstract class Person
{
    public abstract function getName();

    protected abstract function getNumberOfPeople();
}

class Peoples extends Person{
        public function getName(){
        echo "Something from peoples class!";
    }

    public function getNumberOfPeople(){
        echo "Nothing to say...";
    }
}
<?php
$a=10;
$b=8;

echo "a = $a <br/> b = $b";

if ($a==$b)
    echo"<br/><br/>(a==b) = a equal b: True";
else
    echo"<br/><br/>(a==b) = a equal b: False";

if ($a===$b)
    echo"<br/>(a===b) = a & b are different: True";
else
    echo"<br/>(a===b) = a & b are different: True";

if ($a!=$b)
    echo"<br/>(a!=b) = a not equal b: True";
else
    echo"<br/>(a!=b) = a not equal b: False";

if ($a<>$b)
    echo"<br/>(a<>b) = a not equal b: True";
else
    echo"<br/>(a<>b) = a not equal b: False";

if ($a!==$b)
    echo"<br/>(a!==b) = a not not equal b: True";
else
    echo"<br/>(a!==b) = a not not equal b: False";

if ($a>$b)
    echo"<br/>(a>b) = a greater than b: True";
else
    echo"<br/>(a>b) = a greater than b: False";

if ($a<$b)
    echo"<br/>(a<b) = a less than b: True";
else
    echo"<br/>(a<b) = a less than b: False";

if ($a<=$b)
    echo"<br/>(a<=b) = a less than or equal b: True";
else
    echo"<br/>(a<=b) = a less than or equal b: False";

if ($a>=$b)
    echo"<br/>(a>=b) = a greater than or equal b: True";
else
    echo"<br/>(a>=b) = a greater than or equal b: False";